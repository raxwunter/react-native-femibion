import React from 'react';
import {ActivityIndicator, StyleSheet, Text, View,ScrollView ,Image, ListView, Content} from 'react-native';
var Dimensions = require('Dimensions');
var {height, width} = Dimensions.get('window');
import { Ionicons } from '@expo/vector-icons';
import ActionButton from 'react-native-action-button';
import StatusBar from './StatusBar';


export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: true
    }
  }

  componentWillMount() {

    return fetch('https://yzkujqseaa.execute-api.eu-central-1.amazonaws.com/femibiondev/articles?type=BLOG')
      .then((response) => response.json())
      .then((responseJson) => {
        responseJson.map(item => {
          var src = "https://pics.me.me/yo-mama-so-fat-od-that-shescream-that-she-screamed-16113720.png"
          console.log(item.articlePreview);
          const regex  = item.articlePreview.match(/<img .*src="(.*)"/);
          if(regex){

            src = regex[1];
            console.log(src);
          }

          item.articlePreview = src;

        });
        let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        this.setState({
          isLoading: false,
          dataSource: ds.cloneWithRows(responseJson),
        }, function() {
          // do something with new state
        });
      })
      .catch((error) => {
        console.error(error);
      });


  }

  render() {

    if (this.state.isLoading) {
      return (
        <View style={{flex: 1, paddingTop: 20}}>
          <ActivityIndicator />
        </View>
      );
    }
    return (

      <ScrollView style={{flex: 1}}>
        <StatusBar navigation={this.props.navigation}/>
        {/*<StatusBar navigation={this.props.navigation}/>*/}
        <ListView
          dataSource={this.state.dataSource}
          renderRow={(rowData) =>
            <View
              style={styles.container}>
              <Image source={{uri: rowData.articlePreview}} style=
                {{height:height,width:width}} />


              <View style={{ position: 'absolute', backgroundColor:'rgba(0,0,0,.3)' ,height:height , width:width, flex: 1, justifyContent: "flex-end"}}>
                <Text style={{ color: "#fff", padding: 10, fontSize: 22, bottom: 80}}>
                  {rowData.articleTitle}
                </Text>

                <ActionButton
                  buttonColor="rgba(231,76,60,1)"
                  onPress={() => { console.log("hi")}}>
                </ActionButton>
              </View>
            </View >
          }
        />
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  contentContainer: {
    paddingVertical: 20,
    backgroundColor: "#462a75"
  }


});