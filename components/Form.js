import React from 'react';
import {StyleSheet, View, Text, TextInput, Button, Alert, TouchableHighlight} from "react-native";

import StatusBar from './StatusBar';

export class ContactForm extends React.Component {

  _onPressButton() {
    Alert.alert('Thank you for the music, the songs I\'m singing ')
  }


  render() {
    return (
        <View style={styles.container}>
            <TextInput
                style={styles.input}
                ref='first_name'
                label='First Name'
                placeholder='First Name'/>
            <TextInput
                style={styles.input}
                ref='last_name'
                placeholder='Last Name'/>
            <TextInput
                style={styles.input}
                ref='last_name'
                placeholder='Email'/>

          <TouchableHighlight
              style={styles.button}
              color={'#fff'}
              onPress={this._onPressButton}
          >

            <Text> Touch Here </Text>
          </TouchableHighlight>
        </View>
    );
  }
}

const buttonColor = "#e52793";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    width: "80%",
  },
  input: {
    backgroundColor: '#fff',
    color: '#fff',
    width: 200,
    padding: 10,
    marginBottom: 10,
    borderBottomColor: "#e52793"
  },
  button: {
    borderRadius: 20,
    alignItems: 'center',
    backgroundColor: '#e52793',
    justifyContent: 'center',
    padding: 10
  }
});