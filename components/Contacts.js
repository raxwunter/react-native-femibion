import React from 'react';
import { StyleSheet, Text, View, Image } from 'react-native';
import { ContactForm } from "./Form";
import StatusBar from './StatusBar';

export default class App extends React.Component {
  render() {
    return (
        <View style={styles.container}>
        <StatusBar navigation={this.props.navigation}/>
        <Image
            style={styles.logo}
            source={{uri: 'http://d1n8evqmdrmn7s.cloudfront.net/assets/images/myfemibion-logo.png'}}
            resizeMode="contain"/>
        <Text style={styles.title}>Nice to meet you. Hit us up!</Text>

          <ContactForm/>

        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop: 20,
    flex: 1,
    paddingHorizontal: 30,
    backgroundColor: '#462a75',
    alignItems: 'center'
  },
  logo: {
    flex: 1,
    width: 200,
    height: 200,
    alignSelf: 'center',

  },
  title: {
    color: 'white',
    fontSize: 20,
    marginBottom: 30
  }
});
