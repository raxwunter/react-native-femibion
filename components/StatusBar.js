import React from 'react';
import { Ionicons } from '@expo/vector-icons';
import { View, Text, Button, TouchableHighlight} from 'react-native';

export default function StatusBar ({ navigation }) {
  console.log('>>>', navigation);
  return (
  <View style={{position: 'absolute', top: 20, left: 5, zIndex: 100}}>
    <TouchableHighlight
      underlayColor='#462a75'
      activeOpacity={0.5}
      onPress={() => navigation.navigate('DrawerOpen')}
    >
    <Ionicons
      color="white"
      name="ios-menu" size={60}/>
    </TouchableHighlight>
  </View>);
}