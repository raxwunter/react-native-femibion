import React from 'react';
import { View, Text } from 'react-native';
import { DrawerNavigator } from 'react-navigation';
import Contacts from './components/Contacts';
import Articles from './components/Articles';
import StatusBar from './components/StatusBar';
import GoogleMap from './components/GoogleMap';
var Dimensions = require('Dimensions');
var {height, width} = Dimensions.get('window');
//import Profile from './components/Profile';


const HomeScreen = ({ navigation }) => (
  <View style={{backgroundColor: '#462a75', width, height}}>
    <StatusBar navigation={navigation}/>
    <View style={{ flex: 1, paddingTop: 200, height, alignItems: 'center', justifyContent: 'center' }}>
      <Text style={{color: '#fff', fontSize: 40, padding: 10}}>Welcome to Femibion React Native App</Text>
    </View>

  </View>
);

const RootDrawer = DrawerNavigator({
  // 'My Femibion': {
  //   screen: Profile,
  // },
  Home: {
    screen: HomeScreen,
  },
  Feedback: {
    screen: Contacts,
  },
  Location: {
    screen: GoogleMap,
  },
  Articles: {
    screen: Articles
  },
});

export default RootDrawer;